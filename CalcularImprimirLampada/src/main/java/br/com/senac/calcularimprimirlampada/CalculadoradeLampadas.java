/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calcularimprimirlampada;

/**
 *
 * @author sala304b
 */
public class CalculadoradeLampadas {

    int num_lampada; //numero de lampada
    float potencia_lamp;//potencia da lampada
    float largura_com; //largura do comodo
    float comprimento_com;//comprimento do comodo
    float area_com;//area do comodo
    float potot_total;//potencia total

    public CalculadoradeLampadas(int num_lampada, float potencia_lamp, float largura_com, float comprimento_com, float area_com, float potot_total) {
        this.num_lampada = num_lampada;
        this.potencia_lamp = potencia_lamp;
        this.largura_com = largura_com;
        this.comprimento_com = comprimento_com;
        this.area_com = area_com;
        this.potot_total = potot_total;
    }

    public int getNum_lampada() {
        return num_lampada;
    }

    public void setNum_lampada(int num_lampada) {
        this.num_lampada = num_lampada;
    }

    public float getPotencia_lamp() {
        return potencia_lamp;
    }

    public void setPotencia_lamp(float potencia_lamp) {
        this.potencia_lamp = potencia_lamp;
    }

    public float getLargura_com() {
        return largura_com;
    }

    public void setLargura_com(float largura_com) {
        this.largura_com = largura_com;
    }

    public float getComprimento_com() {
        return comprimento_com;
    }

    public void setComprimento_com(float comprimento_com) {
        this.comprimento_com = comprimento_com;
    }

    public float getArea_com() {
        return area_com;
    }

    public void setArea_com(float area_com) {
        this.area_com = area_com;
    }

    public float getPotot_total() {
        return potot_total;
    }

    public void setPotot_total(float potot_total) {
        this.potot_total = potot_total;
    }

   
    }


