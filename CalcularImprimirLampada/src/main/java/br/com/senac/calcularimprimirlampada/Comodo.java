/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calcularimprimirlampada;

/**
 *
 * @author sala304b
 */
public class Comodo {
    private double largura;
    private double complimento;

    public Comodo(double largura, double complimento) {
        this.largura = largura;
        this.complimento = complimento;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getComplimento() {
        return complimento;
    }

    public void setComplimento(double complimento) {
        this.complimento = complimento;
    }

    
    public double getArea(){
        return this.complimento = this.largura;
    
    }
    
}
