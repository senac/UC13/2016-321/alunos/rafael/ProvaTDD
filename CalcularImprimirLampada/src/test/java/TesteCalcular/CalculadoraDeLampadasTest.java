/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TesteCalcular;

import br.com.senac.calcularimprimirlampada.CalculadoradeLampadas;
import br.com.senac.calcularimprimirlampada.Comodo;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author sala304b
 */
public class CalculadoraDeLampadasTest {
@Test
    public void deveUtilizarUmaLampadaDe18wComComodoDeUmMetroQuadado(){
        Comodo comodo = new Comodo(1, 1);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(18, comodo);
        Assert.assertEquals(1, quantidadeLampadas);
  
    }
    @Test
    public void deveUtilizarUmaLampadaDe36wComComodoDeUmMetroQuadado(){
        Comodo comodo = new Comodo(2, 1);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(36, comodo);
        Assert.assertEquals(1, quantidadeLampadas);
    
    }
    
    @Test
    public void deveUtilizarUmaLampadaDe18wComComodoDeUmMetroEMeioQuadado(){
        Comodo comodo = new Comodo(2.5, 1);
        CalculadoradeLampadas calculadoraeLampadas = new CalculadoradeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(18, comodo);
        Assert.assertEquals(1, quantidadeLampadas);
    
    }
    
}